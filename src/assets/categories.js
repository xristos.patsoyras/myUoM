const imgUrl = "https://picsum.photos/1920/1080";

export const Categories = [
  {
    title: "Ακαδημαικό Προσωπικό",
    imgUrl: imgUrl,
    route: "/professors",
  },
];
